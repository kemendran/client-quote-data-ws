package za.co.entelect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientQuoteDataWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientQuoteDataWsApplication.class, args);
	}

}
